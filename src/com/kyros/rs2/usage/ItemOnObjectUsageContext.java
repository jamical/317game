package com.kyros.rs2.usage;

import com.kyros.rs2.model.Location;

/**
 * Represents an item on object usage context. stores info like position of object and object id and item id.
 * @author Jamix
 * @date 15 Oct 2017
 * @time 10:23:30
 *
 */

public class ItemOnObjectUsageContext {

	/**
	 * Constructs a new {@code ItemOnObjectUsageContext} {@code Object}.
	 * @param itemId
	 * @param objectId
	 * @param position
	 */
	public ItemOnObjectUsageContext(int itemId, int objectId, Location location) {
		this.itemId = itemId;
		this.objectId = objectId;
		this.location = location;
	}
	
	/**
	 * The item id of the usage.
	 */
	private final int itemId;
	
	/**
	 * The object id of the usage.
	 */
	private final int objectId;
	
	/**
	 * The location of the object.
	 */
	private final Location location;

	/**
	 * @return the itemId
	 */
	public int getItemId() {
		return itemId;
	}

	/**
	 * @return the objectId
	 */
	public int getObjectId() {
		return objectId;
	}

	/**
	 * @return the location
	 */
	public Location getLocation() {
		return location;
	}
	
	
	
}
