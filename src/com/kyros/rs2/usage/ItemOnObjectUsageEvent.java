package com.kyros.rs2.usage;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.kyros.rs2.model.Player;
import com.kyros.rs2.plugin.Plugin;

/**
 * Represents an item on object usage event.
 * @author Jamix
 * @date 15 Oct 2017
 * @time 10:18:29
 * @param Context. The context of the item on object usage event. It will store data like the object. the item and other data like that.
 */
public abstract class ItemOnObjectUsageEvent<Context extends ItemOnObjectUsageContext> implements Plugin {
	
	/**
	 * Mapping of the data.
	 */
	private static final Map<String,ItemOnObjectUsageEvent> events = new ConcurrentHashMap<>();

	/**
	 * Execute our event. With a defined context.
	 * @param context the context of the event.
	 */
	public abstract void execute(Player player, Context context);
	
	/**
	 * The nodes that trigger the item on object event. 
	 * Index 0: Item Ids.
	 * Index 1: Object Ids.
	 * @return
	 */
	public abstract int[][] nodes();

	/**
	 * 
	 * @return
	 */
	public static Map<String,ItemOnObjectUsageEvent> getEvents() {
		return events;
	}
	
}
