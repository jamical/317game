package com.kyros.rs2.packet;

import com.kyros.rs2.action.impl.MiningAction;
import com.kyros.rs2.action.impl.MiningAction.Node;
import com.kyros.rs2.action.impl.ProspectingAction;
import com.kyros.rs2.action.impl.WoodcuttingAction;
import com.kyros.rs2.action.impl.WoodcuttingAction.Tree;
import com.kyros.rs2.model.Location;
import com.kyros.rs2.model.Player;
import com.kyros.rs2.net.Packet;
import com.kyros.rs2.usage.ItemOnObjectUsageContext;
import com.kyros.rs2.usage.ItemOnObjectUsageEvent;

/**
 * Object option packet handler.
 * @author Graham Edgecombe
 *
 */
public class ObjectOptionPacketHandler implements PacketHandler {
	
	/**
	 * Option 1 opcode.
	 */
	private static final int OPTION_1 = 132, OPTION_2 = 252, ITEM_ON_OBJECT = 192;

	@Override
	public void handle(Player player, Packet packet) {
		switch(packet.getOpcode()) {
		case OPTION_1:
			handleOption1(player, packet);
			break;
		case OPTION_2:
			handleOption2(player, packet);
			break;
		case ITEM_ON_OBJECT:
			itemOnObject(player,packet);
			break;
		}
	}
	
	/**
	 * Handles the item on object packet. Decodes and everything here.
	 * @param player
	 * @param packet
	 */
	private void itemOnObject(Player player, Packet packet) {
		packet.getShortA();//Some unknown short. I think its interface - Jamix
		int objectId = packet.getLEShort();
		int objectY = packet.getLEShortA();
		int itemSlot = packet.getLEShort();
		int objectX = packet.getLEShortA();
		int itemId = packet.getShort();
		if (itemId == player.getInventory().get(itemSlot).getId()) {//no point in going through all the effort to loop everything if the player is cheating or something went wrong wth the container.
			for (ItemOnObjectUsageEvent<ItemOnObjectUsageContext> event : ItemOnObjectUsageEvent.getEvents().values()) {
				for (int node1 : event.nodes()[0]) {
					if (node1 == itemId) {
						for (int node2 : event.nodes()[1]) {
							if (node2 == objectId) {
								//All checks parsed.
								event.execute(player, new ItemOnObjectUsageContext(itemId,objectId,Location.create(objectX, objectY, player.getLocation().getZ())));
								return;
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Handles the option 1 packet.
	 * @param player The player.
	 * @param packet The packet.
	 */
	private void handleOption1(Player player, Packet packet) {
		int x = packet.getLEShortA() & 0xFFFF;
		int id = packet.getShort() & 0xFFFF;
		int y = packet.getShortA() & 0xFFFF;
		Location loc = Location.create(x, y, player.getLocation().getZ());
		// woodcutting
		Tree tree = Tree.forId(id);
		if(tree != null && player.getLocation().isWithinInteractionDistance(loc)) {
			player.getActionQueue().addAction(new WoodcuttingAction(player, loc, tree));
		}
		// mining
		Node node = Node.forId(id);
		if(node != null && player.getLocation().isWithinInteractionDistance(loc)) {
			player.getActionQueue().addAction(new MiningAction(player, loc, node));
		}

	}
	
    /**
     * Handles the option 2 packet.
     * @param player The player.
     * @param packet The packet.
     */
    private void handleOption2(Player player, Packet packet) {        
        int id = packet.getLEShortA() & 0xFFFF;
        int y = packet.getLEShort() & 0xFFFF;
        int x = packet.getShortA() & 0xFFFF;
        Location loc = Location.create(x, y, player.getLocation().getZ());
        Node node = Node.forId(id);
        if(node != null && player.getLocation().isWithinInteractionDistance(loc)) {
            player.getActionQueue().addAction(new ProspectingAction(player, loc, node));
            return;
        }
    }


}
