package com.kyros.rs2.packet;

import com.kyros.rs2.model.Player;
import com.kyros.rs2.net.Packet;

/**
 * Represents the packet handler for doing the next peice of dialogue.
 * @author jamix77
 * @date 16 Oct 2017
 * @time 16:03:35
 *
 */
public class NextDialoguePacketHandler implements PacketHandler {

	@Override
	public void handle(Player player, Packet packet) {
		if (player.getDialogue() == null)
			return;
		player.getDialogue().defaultHandle();
	}

}
