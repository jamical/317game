package com.kyros.rs2.model;

/**
 * Represents a class that has an incrementable element.
 * @author jamix77
 * @date 16 Oct 2017
 * @time 11:35:22
 *
 */
public class Incrementable {
	
	/**
	 * Represents the element to increment.
	 */
	private int element = -1;
	
	/**
	 * Increment the value.
	 * @return
	 */
	public Incrementable increment() {
		setElement(getElement() + 1);
		return this;
	}

	/**
	 * @return the element
	 */
	public int getElement() {
		return element;
	}

	/**
	 * @param element the element to set
	 */
	public void setElement(int element) {
		this.element = element;
	}

}
