package com.kyros.rs2.content.skilling.cooking;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.kyros.rs2.model.Item;

/**
 * Represents an enumeration of cookable food.
 * @author jamix77
 * @created 14:13:30
 * @created 14 Oct 2017
 */
public enum CookableFood {
	/**
	 * Defining the food
	 */
	SHRIMP(new Item(317),new Item(315),new Item(323),1,30,33),
	ANCHOVIES(new Item(321),new Item(319),new Item(323),1,30,34),
	KARAMBWANJI(new Item(3150),new Item(3151),new Item(592),1,10,25),
	SARDINE(new Item(327),new Item(325),new Item(369),1,40,35),
	HERRING(new Item(345),new Item(347),new Item(357),5,50,41),
	MACKEREL(new Item(353),new Item(355),new Item(357),10,60,45),
	TROUT(new Item(335),new Item(333),new Item(343),15,70,50),
	COD(new Item(341),new Item(339),new Item(343),18,75,52),
	PIKE(new Item(349),new Item(351),new Item(343),20,80,59),
	SLIMY_EEL(new Item(3379),new Item(3381),new Item(3383),28,95,58),
	SALMON(new Item(331), new Item(329),new Item(343),25,90,58),
	TUNA(new Item(359),new Item(361),new Item(367),30,100,64),
	CAVE_EEL(new Item(5001),new Item(5003),new Item(5002),38,115,42),
	LOBSTER(new Item(377),new Item(379),new Item(381),40,120,74),
	BASS(new Item(363),new Item(365),new Item(367),43,130,80),
	SWORDFISH(new Item(371),new Item(373),new Item(375),45,140,86),
	MONKFISH(new Item(7944),new Item(7946),new Item(7948),62,150,90),
	SHARK(new Item(383),new Item(385),new Item(387),80,210,99),
	KARAMBWAN(new Item(3142), new Item(3144),new Item(3148),30,190,99)
	;
	
	private static final Map<Integer,CookableFood> DATA = new ConcurrentHashMap<>();
	
	static {
		for (CookableFood food : values()) {
			DATA.put(food.getRaw().getId(), food);
		}
	}
	
	/**
	 * Get a cookable for its id.
	 * @param id
	 * @return
	 */
	public static CookableFood forId(int id) {
		return DATA.get(id);
	}
	
	private Item raw;
	private Item cooked;
	private Item burnt;
	private int level;
	private int exp;
	private int burnlevel;
	private CookableFood(Item raw, Item cooked, Item burnt, int level,int exp,int burnlevel) {
		setRaw(raw);
		setCooked(cooked);
		setLevel(level);
		setExp(exp);
		setBurnt(burnt);
		setBurnlevel(burnlevel);
	}
	public Item getRaw() {
		return raw;
	}
	public void setRaw(Item raw) {
		this.raw = raw;
	}
	public Item getCooked() {
		return cooked;
	}
	public void setCooked(Item cooked) {
		this.cooked = cooked;
	}
	public Item getBurnt() {
		return burnt;
	}
	public void setBurnt(Item burnt) {
		this.burnt = burnt;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getExp() {
		return exp;
	}
	public void setExp(int exp) {
		this.exp = exp;
	}
	public int getBurnlevel() {
		return burnlevel;
	}
	public void setBurnlevel(int burnlevel) {
		this.burnlevel = burnlevel;
	}
	
	
	

}
