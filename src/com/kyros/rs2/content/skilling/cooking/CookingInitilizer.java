package com.kyros.rs2.content.skilling.cooking;

import com.kyros.rs2.model.Item;
import com.kyros.rs2.model.Location;
import com.kyros.rs2.model.Player;
import com.kyros.rs2.usage.ItemOnObjectUsageContext;
import com.kyros.rs2.usage.ItemOnObjectUsageEvent;

/**
 * Represents the skilling initilizer for cooking.
 * @author jamix77
 * @date 14 Oct 2017
 * @time 20:29:36
 *
 */

public class CookingInitilizer extends ItemOnObjectUsageEvent<ItemOnObjectUsageContext> {

	@Override
	public void onStartup() {}

	@Override
	public String getIdentifier() {
		return "cookingInitilizer";
	}

	@Override
	public void execute(Player player, ItemOnObjectUsageContext context) {
		Item item = new Item(context.getItemId());
		int objectId = context.getObjectId();
		Location location = context.getLocation();
		CookableFood food = CookableFood.forId(item.getId());
		CookingPulse cookingPulse = new CookingPulse();
		cookingPulse.setPlayer(player);
		cookingPulse.initilize(food,objectId,location,5);
		player.startPulse(cookingPulse);
	}

	@Override
	public int[][] nodes() {
		return new int[][] {{317},{1281}};
	}


}
