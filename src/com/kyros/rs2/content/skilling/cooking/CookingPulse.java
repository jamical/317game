package com.kyros.rs2.content.skilling.cooking;

import java.security.SecureRandom;

import com.kyros.rs2.content.skilling.Skill;
import com.kyros.rs2.model.Animation;
import com.kyros.rs2.model.GameObjectDefinition;
import com.kyros.rs2.model.Location;
import com.kyros.rs2.pulse.Pulse;

/**
 * Represents the cooking pulse.
 * @author jamix77
 * @created 16:19:14
 * @created 14 Oct 2017
 */
public class CookingPulse extends Pulse {
	
	/**
	 * Represents the animation for cooking on a range in Kyros.
	 */
	final Animation RANGE_ANIM = Animation.create(883);
	
	/**
	 * Represents the animation for cooking on a fire in Kyros.
	 */
	final Animation FIRE_ANIM = Animation.create(897);
	
	/**
	 * The cookable food we are doing.
	 */
	CookableFood food;
	
	/**
	 * The id of the object.
	 */
	int objectId;
	
	/**
	 * The cycles left.
	 */
	int cyclesLeft = 0;
	
	/**
	 * The location of the object in game.
	 */
	Location objectLocation;

	@Override
	public void onStartup() {}

	@Override
	public String getIdentifier() {
		return "cookingPulse";
	}


	@Override
	public boolean execute() {
		if (cyclesLeft-- <= 0) {
			return true;
		}
		if (getPlayer().getSkills().getLevel(Skill.COOKING.getId()) < food.getLevel()) {
			getPlayer().getActionSender().sendMessage("You need a level of " + food.getLevel() + " to cook that.");
			return true;
		}
		if (getPlayer().getInventory().contains(food.getRaw().getId()) == false) {
			getPlayer().getActionSender().sendMessage("You have run out of fish to cook.");
			return true;
		}
		if (getTicks() % (GameObjectDefinition.forId(objectId).getName().contains("fire") ? 4 : 5) != 0) {
			return false;
		}
		getPlayer().playAnimation(GameObjectDefinition.forId(objectId).getName().contains("fire") ? FIRE_ANIM : RANGE_ANIM);
		getPlayer().getInventory().remove(food.getRaw());
		boolean burnt = isBurned();
		getPlayer().getInventory().add(burnt ? food.getBurnt() : food.getCooked());
		getPlayer().getSkills().addExperience(Skill.COOKING.getId(), burnt ? 0 : food.getExp());
		return cyclesLeft <= 0;
	}
	
	/**
	 * Did we burn the food?
	 * @return
	 */
	private boolean isBurned() {
		SecureRandom RANDOM = new SecureRandom();
		if (getPlayer().getSkills().getLevel(Skill.COOKING.getId()) > food.getBurnlevel() && food.getRaw().getId() != 11934) {
			return false;
		}
		double burn_chance = (55.0 - (GameObjectDefinition.forId(objectId).getName().toLowerCase().equals("fire") ? 0.0 : 3.0));
		double cook_level = (double) getPlayer().getSkills().getLevel(Skill.COOKING.getId());
		double lev_needed = (double) food.getLevel();
		double burn_stop = (double) food.getBurnlevel();
		double multi_a = (burn_stop - lev_needed);
		double burn_dec = (burn_chance / multi_a);
		double multi_b = (cook_level - lev_needed);
		burn_chance -= (multi_b * burn_dec);
		double randNum = RANDOM.nextDouble() * 100.0;
		return burn_chance <= randNum ? false : true;
	}

	/* (non-Javadoc)
	 * @see com.kyros.rs2.pulse.Pulse#initilize(java.lang.Object[])
	 */
	@Override
	public void initilize(Object... objects) {
		food = (CookableFood) objects[0];
		objectId = (int) objects[1];
		objectLocation = (Location) objects[2];
		cyclesLeft = (Integer) objects[3];
	}

	/* (non-Javadoc)
	 * @see com.kyros.rs2.pulse.Pulse#walkability()
	 */
	@Override
	public Walkability walkability() {
		return Walkability.NON_WALKABLE;
	}

}
