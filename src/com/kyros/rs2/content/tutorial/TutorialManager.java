package com.kyros.rs2.content.tutorial;

import com.kyros.rs2.model.Player;

/**
 * Represents the manager for tutorial island.
 * @author jamix77
 * @date 16 Oct 2017
 * @time 10:31:09
 *
 */
public class TutorialManager {
	
	/**
	 * The player.
	 */
	private final Player player;
	
	/**
	 * The stage of the tutorial manager
	 */
	private int stage = 0;
	
	/**
	 * 
	 * Constructs a new {@code TutorialManager} {@code Object}.
	 * @param player
	 */
	public TutorialManager(final Player player) {
		this.player = player;
	}
	
	/**
	 * Do the bit of tutorial staged by the stage variable.
	 */
	public void doTutorial() {
		switch (stage) {
		case 0:
			stage++;//Skip char design for now.
			break;
		case 1:
			//First dialogue from the rs guide.
			break;
		}
	}

	/**
	 * @return the player
	 */
	public final Player getPlayer() {
		return player;
	}

	/**
	 * @return the stage
	 */
	public int getStage() {
		return stage;
	}

	/**
	 * @param stage the stage to set
	 */
	public void setStage(int stage) {
		this.stage = stage;
	}


}
