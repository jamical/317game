package com.kyros.rs2.content.dialogue;

import com.kyros.rs2.model.NPC;
import com.kyros.rs2.model.Player;

/**
 * Represents the interpreter for a peice of dialogue.
 * @author jamix77
 * @date 16 Oct 2017
 * @time 11:42:31
 *
 */
public class DialogueInterpreter {
	
	/**
	 * The dialogue that this interpreter is held in.
	 */
	private Dialogue dialogue;
	
	/**
	 * Represents the player in the dialogue.
	 * TODO Add mutliplayered dialogue.
	 */
	private Player player;
	
	/**
	 * 
	 * Constructs a new {@code DialogueInterpreter} {@code Object}.
	 * @param player
	 * @param dialogue
	 */
	public DialogueInterpreter(Player player, Dialogue dialogue) {
		this.setPlayer(player);
		this.setDialogue(dialogue);
	}
	
	/**
	 * Sends an npc dialogue. This is with more detail.
	 * @param npc the npc.
	 * @param e the expression on the npcs face.
	 * @param lines the lines of dialogue.
	 * @return the interpreter instance.
	 */
	public DialogueInterpreter npc(NPC npc,Expression e,String...lines) {
        int id = NPC_IDS[lines.length-1];
		player.getActionSender().sendChatInterface(id);
        player.getActionSender().sendString(id + 2, "NPC #" + npc.getDefinition().getId());
        player.getActionSender().sendNPCDialogueHead(npc.getIndex(), id + 1);
        player.getActionSender().sendDialogueAnimation(id + 1, e.getAnimation().getId());
		for (int i = 0; i < lines.length; i++)
			player.getActionSender().sendString(id + 3 + i, lines[i]);
		return this;
	}
	
	/**
	 * Sends a player dialogue. 
	 * @param e the expression on the face of the player.
	 * @param dialogues the strings of dialogue.
	 * @return
	 */
	public DialogueInterpreter sendPlayerChat(Expression e, String... dialogues) {
		int id = PLAYER_IDS[dialogues.length-1];
		player.getActionSender().sendString(id + 2, player.getName());
		player.getActionSender().sendDialogueAnimation(id + 1, e.getAnimation().getId());
		player.getActionSender().sendPlayerDialogueHead(id + 1);
		player.getActionSender().sendChatInterface(id);
		for (int i = 0; i < dialogues.length; i++)
			player.getActionSender().sendString(id + 3 + i, dialogues[i]);
		return this;
	}
	
	/**
	 * Sends a series of options for the player to interact with.
	 * @param options
	 * @return
	 */
	public DialogueInterpreter sendOptions(String... options) {
		int id = OPTION_IDS[options.length-1];
		player.getActionSender().sendString(id + 1, "Select an Option");
		for (int i = 0; i < options.length; i++)
			player.getActionSender().sendString(id + 2 + i, options[i]);
		player.getActionSender().sendChatInterface(id);
		return this;
	}
	
	/**
	 * Sends a statement.
	 * @param p The player.
	 * @param lines The lines of dialogue for the statement.
	 */
	public DialogueInterpreter sendStatement(String... lines) {
		int id = STATEMENT_IDS[lines.length-1];
		for (int i = 0; i < lines.length; i++)
			player.getActionSender().sendString(id + 1 + i, lines[i]);
		player.getActionSender().sendChatInterface(id);
		return this;
	}
	
	private static int[] NPC_IDS = {4882, 4887, 4893, 4900};
	private static int[] PLAYER_IDS = {968, 973, 979, 986};
	private static int[] OPTION_IDS = {2459, 2469, 2480, 2492};
	private static int[] STATEMENT_IDS = {356, 359, 363, 368, 374};

	/**
	 * @return the dialogue
	 */
	public Dialogue getDialogue() {
		return dialogue;
	}

	/**
	 * @param dialogue the dialogue to set
	 */
	public void setDialogue(Dialogue dialogue) {
		this.dialogue = dialogue;
	}

	/**
	 * @return the player
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * @param player the player to set
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}

}
