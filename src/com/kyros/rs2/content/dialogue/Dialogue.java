package com.kyros.rs2.content.dialogue;

import com.kyros.rs2.model.Incrementable;
import com.kyros.rs2.model.Player;

/**
 * Represents a peice of dialogue. Will use same style of design as my released on rune-server.
 * https://www.rune-server.ee/runescape-development/rs2-server/snippets/663463-elvarg-osrspk-improved-dialogue-system.html
 * 
 * @author jamix77
 * @date 16 Oct 2017
 * @time 11:33:00
 *
 */
public abstract class Dialogue {

	/**
	 * The player.
	 */
	protected final Player player;
	
	/**
	 * Represents an incrementable value which stores our stage.
	 */
	protected final Incrementable stage = new Incrementable();
	
	/**
	 * Represents the interpreter for the dialgue.
	 */
	protected final DialogueInterpreter interpreter;

	/**
	 * Handle our dialogue.
	 * @param option the button id. if none was chosen its -1.
	 * @return
	 */
	public abstract boolean handle(int option);
	
	/**
	 * Defaultly handle our stuff.
	 * The option was not supplied so will by default be -1. When it interpretes -1 it says
	 */
	public void defaultHandle() {
		defaultHandle(-1);
	}
	
	/**
	 * Defaultly handle the stuff. With a supplied option.
	 * @param option
	 */
	public void defaultHandle(int option) {
		if (handle(stage.increment().getElement())) {
			stop();
		}
	}
	
	/**
	 * Stop the dialogue.
	 */
	public void stop() {
		player.setDialogue(null);
		player.getActionSender().sendIntefaceRemoval();
	}
	
	/**
	 * Start a certain dialogue.
	 */
	public static void startDialogue(Player player, Dialogue dialogue) {
		player.setDialogue(dialogue);
		dialogue.defaultHandle();
	}
	
	/**
	 * 
	 * Constructs a new {@code Dialogue} {@code Object}.
	 * @param player
	 */
	public Dialogue(Player player) {
		this.player = player;
		this.interpreter = new DialogueInterpreter(player,this);
	}
	
}
