package com.kyros.rs2.task.impl;

import com.kyros.rs2.GameEngine;
import com.kyros.rs2.task.Task;

/**
 * A task which stops the game engine.
 * @author Graham Edgecombe
 *
 */
public class DeathTask implements Task {

	@Override
	public void execute(GameEngine context) {
		if(context.isRunning()) {
			context.stop();
		}
	}

}
