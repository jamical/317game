package com.kyros.rs2.task.impl;

import org.apache.mina.core.session.IoSession;

import com.kyros.rs2.GameEngine;
import com.kyros.rs2.net.Packet;
import com.kyros.rs2.net.PacketManager;
import com.kyros.rs2.task.Task;

/**
 * A task that is executed when a session receives a message.
 * @author Graham Edgecombe
 *
 */
public class SessionMessageTask implements Task {
	
	/**
	 * The session.
	 */
	private IoSession session;
	
	/**
	 * The packet.
	 */
	private Packet message;

	/**
	 * Creates the session message task.
	 * @param session The session.
	 * @param message The packet.
	 */
	public SessionMessageTask(IoSession session, Packet message) {
		this.session = session;
		this.message = message;
	}

	@Override
	public void execute(GameEngine context) {
		PacketManager.getPacketManager().handle(session, message);
	}

}
