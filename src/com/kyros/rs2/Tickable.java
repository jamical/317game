package com.kyros.rs2;

import java.util.LinkedList;
import java.util.List;

import org.python.modules.time.Time;

import com.kyros.rs2.pulse.Pulse;

/**
 * This represents the engine but in a tickable fashion. It runs every 1 tick. Which is 600ms.
 * @author jamix77
 *
 */
public class Tickable implements Runnable {
	
	/**
	 * A list of pulses.
	 */
	private static final List<Pulse> pulses = new LinkedList<>();
	
	/**
	 * A list of pulses that need to be added to the executable list of current pulses.
	 */
	private static final List<Pulse> pulsesToBeAdded = new LinkedList<>();
	
	/**
	 * A list of pulses that need to be removed from the executable list of current pulses.
	 */
	private static final List<Pulse> pulsesToBeRemoved = new LinkedList<>();
	
	/**
	 * Submit a pulse to be ran.
	 * @param pulse
	 */
	public static void submitPulse(Pulse pulse) {
		pulsesToBeAdded.add(pulse);
	}
	
	/**
	 * Remove a pulse
	 * @param pulse the pulse
	 * @return if the pulse was even in the list and was removed.
	 */
	public static boolean removePulse(Pulse pulse) {
		return pulsesToBeRemoved.add(pulse);
	}
	
	/**
	 * The thread this tickable is running on.
	 */
	private Thread thread;
	
	/**
	 * Get a new tickable.
	 * @param start
	 * @return
	 */
	public static Tickable newTickable(boolean start) {
		Tickable tickable = new Tickable();
		Thread thread = new Thread(tickable);
		tickable.setThread(thread);
		if (start)
			thread.start();
		return tickable;
	}
	
	@Override
	public void run() {
		while (true) {
			long cached = System.currentTimeMillis();
			for (Pulse pulse : pulses) {
				if (pulse.isDisabled()) {
					pulsesToBeRemoved.add(pulse);
					continue;
				}
				if (pulse.execute()) {
					pulsesToBeRemoved.add(pulse);
				}
				pulse.setTicks(pulse.getTicks() + 1);
			}
			pulses.removeAll(pulsesToBeRemoved);
			pulses.addAll(pulsesToBeAdded);
			pulsesToBeRemoved.clear();
			pulsesToBeAdded.clear();
			try {
				Thread.sleep(600 - (System.currentTimeMillis() - cached));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

	public Thread getThread() {
		return thread;
	}

	public void setThread(Thread thread) {
		this.thread = thread;
	}


}
