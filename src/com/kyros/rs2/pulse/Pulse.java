package com.kyros.rs2.pulse;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.kyros.rs2.model.Player;
import com.kyros.rs2.plugin.Plugin;

/**
 * Represents a single ticking pulse. Will be executed every single tick.
 * @author jamix77
 * @created 14:13:08
 * @created 14 Oct 2017
 */
public abstract class Pulse implements Plugin,Cloneable {
	
	/**
	 * Represents the pulses walkablity.
	 * @author jamix77
	 * @created 15:04:01
	 * @created 14 Oct 2017
	 */
	public static enum Walkability {
		
		/**
		 * The player can walk.
		 * When walking the pulse will continue to work.
		 */
		WALKABLE,
		
		/**
		 * The player cant walk;
		 * The pulse will end and be removed if this is so.
		 */
		NON_WALKABLE;
	}
	
	/**
	 * The pulses.
	 */
	private static final Map<String,Pulse> pulses = new ConcurrentHashMap<>();
	
	/**
	 * The player
	 */
	private Player player;
	
	/**
	 * The amount of ticks passed.
	 */
	private int ticks = 0;
	
	/**
	 * is the pulse disabled and needs to be removed?
	 */
	private boolean disabled = false;
	
	/**
	 * Mapping of attributes.
	 */
	private Map<String,Object> attributes = new ConcurrentHashMap<>();
	
	/**
	 * Execute the event.
	 * @return
	 */
	public abstract boolean execute();
	
	/**
	 * Initilize our pulse. Not start it up but set attributes.
	 * @param objects
	 */
	public abstract void initilize(Object...objects);
	
	/**
	 * Is this pulse's walkability allow for walking during the pulse or not?
	 * @return
	 */
	public abstract Walkability walkability();

	public int getTicks() {
		return ticks;
	}

	public void setTicks(int ticks) {
		this.ticks = ticks;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public static Map<String,Pulse> getPulses() {
		return pulses;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Map<String,Object> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String,Object> attributes) {
		this.attributes = attributes;
	}

}
