package com.kyros.rs2.plugin;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;

import com.kyros.rs2.pulse.Pulse;
import com.kyros.rs2.usage.ItemOnObjectUsageEvent;

/**
 * Represents the plugin manager.
 * @author jamix77
 *
 */
public class PluginManager {
	
	/**
	 * Represents the root of the plugins.
	 */
	private static final File root = new File("./data/plugin/");

	/**
	 * The amount of plugins loaded.
	 */
	private int plugins = 0;
	
	/**
	 * Hot fixes the plugins.
	 * This means the plugins will be reloaded during the running of the game. This is useful for finding bugs, patching them, then quickly releasing the fix.
	 * This allows for plugin rebooting and not having to shut offline game because of 1 small bug or something. Best way to do it.
	 * @throws Throwable 
	 */
	public void hotfix() throws Throwable {
		//CLEAR PLUGINS HERE.
		load();
	}
	
	/**
	 * Load the plugins.
	 * @throws Throwable 
	 */
	public void load() throws Throwable {
		load(root);
		Logger.getLogger(getClass().getName()).info("Loaded " + plugins + " plugins.");
	}
	
	/**
	 * Load the plugins
	 * @param root the directory to load from.
	 */
	public void load(File root) throws Throwable {
		URLClassLoader loader;
		for (File subdirectory : root.listFiles()) {
			if (subdirectory.isDirectory()) {
				load(subdirectory);
				continue;
			}
			String fileName = subdirectory.getName().replace(".jar", "").replace(".class", "");
			loader = new URLClassLoader(new URL[] {root.toURI().toURL(),subdirectory.toURI().toURL()});
			JarFile jar = new JarFile(subdirectory);
			Enumeration<JarEntry> entries = jar.entries();
			while (entries.hasMoreElements()) {
				JarEntry entry = entries.nextElement();
				if (entry.getName().endsWith(fileName+".class")) {
					StringBuilder stringBuilder = new StringBuilder();
					for (String path : entry.getName().split("/")) {
						if (stringBuilder.length() != 0) {
							stringBuilder.append(".");
						}
						stringBuilder.append(path);
						if (path.endsWith(".class")) {
							stringBuilder.setLength(stringBuilder.length() -6);
							break;
						}
					}
					try {
						final Plugin plugin = (Plugin) loader.loadClass(stringBuilder.toString()).newInstance();
						initilizePlugin(plugin);
					} catch (Throwable t) {
						t.printStackTrace(System.err);
					}
				}
			}
			jar.close();
		}
	}
	
	/**
	 * Initilize the plugin.
	 * @param plugin
	 */
	public void initilizePlugin(Plugin plugin){
		plugin.onStartup();
		if (plugin instanceof Pulse) {
			Pulse.getPulses().put(plugin.getIdentifier(),((Pulse)plugin));
		}
		if (plugin instanceof ItemOnObjectUsageEvent) {
			ItemOnObjectUsageEvent.getEvents().put(plugin.getIdentifier(), (ItemOnObjectUsageEvent)plugin);
		}
		plugins++;
		
	}
	
}
