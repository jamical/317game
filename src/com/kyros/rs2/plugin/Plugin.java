package com.kyros.rs2.plugin;

/**
 * Represents a plugin.
 * This is the base for the plugin. All extensions are still classified as a plugin and will be treated as such along side its extra features.
 * Plugins will be player owned when initilized through the player.
 * @author jamix77
 *
 */
public interface Plugin {

	/**
	 * Called when the game starts up.
	 */
	public abstract void onStartup();
	
	/**
	 * Represents the identifier, this is used for mapping.
	 * @return
	 */
	public abstract String getIdentifier();
	
}
