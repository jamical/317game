package com.kyros;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.kyros.fileserver.FileServer;
import com.kyros.rs2.RS2Server;
import com.kyros.rs2.Tickable;
import com.kyros.rs2.model.World;
import com.kyros.rs2.plugin.PluginManager;
import com.kyros.rs2.pulse.Pulse;

/**
 * A class to start both the file and game servers.
 * @author Graham Edgecombe
 *
 */
public class Server {
	
	/**
	 * The protocol version.
	 */
	public static final int VERSION = 317;
	
	/**
	 * Logger instance.
	 */
	private static final Logger logger = Logger.getLogger(Server.class.getName());
	
	/**
	 * The context of the game.
	 */
	private static KyrosContext context = new KyrosContext("Kyros",43594);
	
	/**
	 * The entry point of the application.
	 * @param args The command line arguments.
	 */
	public static void main(String[] args) {
		if (args.length == 2) {
			context = new KyrosContext(args[0],Integer.parseInt(args[1]));
		}
		logger.info("Starting "+context.getName()+"...");
		World.getWorld(); // this starts off background loading
		try {
			new FileServer().bind().start();
			new RS2Server().bind(context.getPort()).start();
			Tickable.newTickable(true);
			new PluginManager().load();
		} catch(Exception ex) {
			logger.log(Level.SEVERE, "Error starting "+context.getName()+".", ex);
			System.exit(1);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static KyrosContext getContext() {
		return context;
	}

	public static void setContext(KyrosContext context) {
		Server.context = context;
	}

}
