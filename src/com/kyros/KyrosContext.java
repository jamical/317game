package com.kyros;

/**
 * Represents the context of kyros at the moment.
 * @author jamix77
 * @date 15 Oct 2017
 * @time 20:35:36
 *
 */
public class KyrosContext {
	
	/**
	 * The port of the server.
	 */
	private int port;
	
	/**
	 * The name of the server.
	 */
	private String name;
	
	/**
	 * 
	 * Constructs a new {@code KyrosContext} {@code Object}.
	 * @param name
	 * @param port
	 */
	public KyrosContext(String name, int port) {
		this.name = name;
		this.port = port;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	

}
